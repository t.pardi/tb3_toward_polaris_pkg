# Turtlebot3 toward Polaris Pkg

This ROS-kinetic package changes the orientation of turtlebot3 robot toward Polaris (the north star). 

## Requirements
Ros packages:
- ros-kinetic-turtlebot3-*

## Usage
Load the robot model running:
```
roslaunch tb3_toward_polaris_pkg turtlebot3_gazebo_sim.launch
```
It runs an empty gazebo model and spawns the robot at the centre of the map. The initial pose (position and orientation) can be modified using the args in the launch file.

You can change the model of turtlebot3 via the environment variable TURTLEBOT3_MODEL (`waffle`, `burger`, `waffle_pi` are available and tested), such as:
```
export TURTLEBOT3_MODEL=waffle
``` 

Load the controller:
```
roslaunch tb3_toward_polaris_pkg turtlebot3_gazebo_control.launch
```
It starts to control the heading of the robot and rotates it toward Polaris. You can select the PID gains by changing the arguments in the launch file.




