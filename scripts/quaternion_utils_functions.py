import tf

#--------------------------------------------------
# Invert a quaternion
#--------------------------------------------------

def quaternionInv(q):
	q_inv = (q.x, q.y, q.z, -q.w)

	return q_inv

#--------------------------------------------------
# Compute the relative rotation between two 
# quaternions
#--------------------------------------------------

def relativeRotation(q1, q2):
	
	q1_inv = quaternionInv(q1)

	qr = tf.transformations.quaternion_multiply((q2.x, q2.y, q2.z, q2.w), q1_inv)

	return qr
