#!/usr/bin/env python
import TB3_ctrl

if __name__ == '__main__':

	# Instantiate the controller

	tb3_ctrl = TB3_ctrl.TB3_ctrl()

	# Spin - Infinite Loop!
	tb3_ctrl.spin()
