import rospy
import tf

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Twist 
from geometry_msgs.msg import Quaternion

from threading import Thread, Lock

from quaternion_utils_functions import *

import sys

#################################################
#
# Class for controllint the TurtleBot3
#
#################################################

class TB3_ctrl:
	# Variables
	_q_des = Quaternion(0, 0, 0, 1)
	_curr_q = Quaternion(0, 0, 0, 1)

	# PID gains
	_Kp = 1
	_Kd = .01
	_Ki = .05

	# Sync vars
	_mutex = Lock()


# ----> Private methods

	#--------------------------------------------------
	# Constructor
	#--------------------------------------------------

	def __init__(self, q_des = Quaternion(0, 0, 0, 1)):
		
		rospy.init_node('TB3_control', anonymous=True)

		myargv = rospy.myargv(argv=sys.argv)

		if (len(myargv) != 4):
			rospy.logerr("Not enough inputs! One ore more gains are missing!")
			sys.exit(1)


		# Set the gains
		self.__setGains(float(myargv[1]), float(myargv[2]), float(myargv[3]))

		self._q_des = q_des

	#--------------------------------------------------
	# Callback function to get the IMU data from the 
	# robot
	#--------------------------------------------------

	def __callbackIMU(self, msg):

		self._mutex.acquire()

		# Store the current orientation
		
		self._curr_q = msg.orientation 
		
		self._mutex.release()

	#--------------------------------------------------
	# Start the subscriber for /imu
	#--------------------------------------------------

	def __initSubscriber(self):

		rospy.Subscriber("/imu", Imu, self.__callbackIMU)

	#--------------------------------------------------
	# Thread: function to send the command to the robot
	#--------------------------------------------------

	def __thread_function(self, name):

		# Initialise the publisher for sending the commands to the turtlebot
		pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

		# Set frequency in Hz
		rate = rospy.Rate(10)
		dt = .1;
		
		# Define a message
		msg = Twist()	

		# Vars for integral and derivative components
		e_yaw_old = 0
		e_yaw_int = 0

		# Start main loop
		while not rospy.is_shutdown():

			# Save the current heading

			self._mutex.acquire()

			q_target = self._curr_q

			self._mutex.release()

			# Compute the relative error as quaternion
			e_q = relativeRotation(self._q_des, q_target)

			# Get the euler angle from the quaternion
			euler = tf.transformations.euler_from_quaternion(e_q)	  
		
			# Compose errors
			e_p = euler[2] 
			e_i = e_yaw_int + e_p
			e_d = (e_p - e_yaw_old) / dt

			# Compute the new command
			msg.angular.z = - self._Kp * e_p - self._Ki * e_i - self._Kd * e_d

			# Save data for the integral and derivative components
			e_yaw_old = e_p
			e_yaw_int = e_i

			# Publish the message
			pub.publish(msg)

			# Sleep 
			rate.sleep()

	def __setGains(self, Kp, Ki, Kd):
		self._Kp = Kp
		self._Ki = Ki
		self._Kd = Kd


# ----> Public methods

	def spin(self):

		rospy.loginfo("Load the subscriber...")
		
		self.__initSubscriber()

		t = Thread(target=self.__thread_function, args=(1,))

		rospy.loginfo("Start the controller...")

		t.start()

		# Start the loop
		rospy.loginfo("Start main loop.")
		rospy.spin()